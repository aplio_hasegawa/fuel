<?php
use Fuel\Core\Form;

?>
<div>
    <?= Form::open(['action' => '/chat/index', 'method' => 'post']) ?>
    <div class="form-group">
        <label for="message">メッセージ</label>
        <?= html_tag('input', [
            'id' => 'message',
            'name' => 'message',
            'type' => 'text',
            'class' => 'form-control',
            'placeholder' => 'Message'
        ]) ?>
    </div>
    <div class="form-group">
        <?= html_tag("input", ['id' => 'submit', 'type' => 'submit', 'class' => 'btn btn-default'], '登録'); ?>
    </div>
    <?= Form::close(); ?>

    <div class="form-group">
        <ul class="list-group">
            <?php foreach ($messages as $message) : ?>
                <li class="list-group-item">
                    <?= $message ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>