<?php

use Fuel\Core\Controller_Template;
use Fuel\Core\File;
use Fuel\Core\Input;
use Fuel\Core\Response;
use Fuel\Core\Session;
use Fuel\Core\Validation;
use Fuel\Core\View;

class Controller_Chat extends Controller_Template
{
    const CHAT_FILE_NAME = "chat.txt";

    public function get_index()
    {

        $messages = [];
        if (File::exists(DOCROOT . self::CHAT_FILE_NAME)) {
            $currentContents = File::read(DOCROOT . "chat.txt", true);
            $messages = explode("\n", $currentContents);
        }

        $this->template->title = "チャットサンプル";
        $this->template->content =
            View::forge('chat/index')
                ->set('subnav', ['index' => 'active'])
                ->set('messages', $messages);
    }

    public function post_index()
    {
        $validation = Validation::forge();
        $validation->add('message', 'メッセージ')
            ->add_rule("required");
        if (!$validation->run()) {
            Session::set_flash('error', $validation->error_message());
            Response::redirect("/chat/");
        }

        $message = Input::post("message");

        if (!File::exists("chat.txt")) {
            File::update(DOCROOT, self::CHAT_FILE_NAME, $message);
        } else {
            $currentContents = File::read(DOCROOT . self::CHAT_FILE_NAME, true);
            $newContents = $message . "\n" . $currentContents;
            File::update(DOCROOT, "chat.txt", $newContents);
        }

        Response::redirect("/chat/");
    }

}
